import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/*

This program runs a Monte Carlo Simulation on a investment using past risk (SD) and
return (mean) to predict outcomes over future time periods.

We use a pseudo-random generator to predict how the investment will perform every year.
For example: Say past risk (SD) is 15 and return (mean) is 9. We generate a random number between 0 and 80.
If the number is between 0 and 15, then we simulate that the investment will result in a loss.
On the other hand, if the random number is between 16 to 80, we simulate that the investment results in profit of 9% yearly.
We continue this simulation for 20 years and for 10000 times and print the median, 10th percentile and 20th percentile at the end.

With this program, we observe that aggressive portfolio yields can be much higher or much lower than the conservative portfolio.

*/

public class ModernPortfolioTheory {

    private static final Integer NUMBER_OF_SIMULATIONS = 10000;
    private static final Integer YEARS = 20;
    private static final Double INITIAL_INVESTMENT = 100000.0d;
    private static final Double AGGRESSIVE_RETURN = 9.4324d / 100;
    private static final Double CONSERVATIVE_RETURN = 6.189d / 100;
    private static final Double AGGRESSIVE_RISK = 15.675d;
    private static final Double CONSERVATIVE_RISK = 6.3438d;
    private static final Integer MEDIAN = 5000;
    private static final Integer TEN_PERCENT_BEST_CASE = 9000;
    private static final Integer TEN_PERCENT_WORST_CASE = 1000;
    private static Random rand = new Random();

    public static void main(String[] args) {

        ArrayList<Double> aggressiveList = new ArrayList<>();
        ArrayList<Double> conservativeList = new ArrayList<>();

        getReturn(aggressiveList, AGGRESSIVE_RETURN, AGGRESSIVE_RISK);
        getReturn(conservativeList, CONSERVATIVE_RETURN, CONSERVATIVE_RISK);

        Collections.sort(aggressiveList);
        Collections.sort(conservativeList);

        System.out.println("------------- Aggressive -------------");
        System.out.println("Median 20th Year " + aggressiveList.get(MEDIAN).intValue());
        System.out.println("10 % Best Case " + aggressiveList.get(TEN_PERCENT_BEST_CASE).intValue());
        System.out.println("10 % Worst Case " + aggressiveList.get(TEN_PERCENT_WORST_CASE).intValue());

        System.out.println("------------- Very Conservative -------------");
        System.out.println("Median 20th Year " + conservativeList.get(MEDIAN).intValue());
        System.out.println("10 % Best Case " + conservativeList.get(TEN_PERCENT_BEST_CASE).intValue());
        System.out.println("10 % Worst Case " + conservativeList.get(TEN_PERCENT_WORST_CASE).intValue());

    }

    private static void getReturn(ArrayList<Double> aggressiveList, Double yearReturn, Double risk) {
        for (int i = 0; i < NUMBER_OF_SIMULATIONS; i++) {
            aggressiveList.add(calculateReturnForTwentyYear(yearReturn, risk));
        }
    }

    private static Double calculateReturnForTwentyYear(Double yearReturn, Double risk) {
        Double result = INITIAL_INVESTMENT;
        for (int i = 0; i < YEARS; i++) {
            int randomValue = rand.nextInt(80);
            if (randomValue >= 0 && randomValue <= risk) {
                result = result - (result * yearReturn);
            } else {
                result = result + (result * yearReturn);
            }
        }
        return result;
    }
}
